<?php
$nilai = 80;
$pesan = "";

if ($nilai >= 60) {
    $pesan = "Selamat! Nilai Anda $nilai. Anda Lulus 🎉";
    $status = "pass";
} else {
    $pesan = "Maaf, Nilai Anda $nilai. Anda Belum Lulus 😢";
    $status = "fail";
}
?>
<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hasil Ujian</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;700&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Nunito', sans-serif;
            background: linear-gradient(to right, #1f4037, #99f2c8);
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
            color: #333;
        }
        .container {
            background-color: rgba(255, 255, 255, 0.95);
            padding: 40px;
            border-radius: 15px;
            text-align: center;
            box-shadow: 0 10px 30px rgba(0, 0, 0, 0.15);
            max-width: 500px;
            width: 100%;
        }
        h1 {
            margin-bottom: 20px;
            font-size: 2em;
        }
        .message {
            font-size: 1.5em;
            margin: 20px 0;
        }
        .pass {
            color: #2ecc71;
        }
        .fail {
            color: #e74c3c;
        }
        .retry-button {
            background-color: #27ae60;
            color: #fff;
            padding: 12px 25px;
            border: none;
            border-radius: 25px;
            font-size: 1em;
            cursor: pointer;
            transition: all 0.3s ease;
        }
        .retry-button:hover {
            background-color: #2ecc71;
            transform: scale(1.05);
        }
        .animation {
            animation: bounce 2s infinite;
        }
        @keyframes bounce {
            0%, 20%, 50%, 80%, 100% {
                transform: translateY(0);
            }
            40% {
                transform: translateY(-30px);
            }
            60% {
                transform: translateY(-15px);
            }
        }
    </style>
</head>
<body>
    <div class="container animation">
        <h1>Hasil Ujian Anda</h1>
        <p class="message <?php echo $status; ?>">
            <?php echo $pesan; ?>
        </p>
        <button class="retry-button" onclick="window.location.reload();">Cek Nilai Lagi</button>
    </div>
</body>
</html>
